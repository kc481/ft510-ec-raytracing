I am starting to work through Ray Tracing in One Weekend.

Chapter 1：
Knowing something about raytracing.

Chapter 2:
Using Auto as a type when the type of data is not yet identified.
Static_cast to do hard cast of data type.
Add a process indicator to prevent infinite roop.

Chapter 3.1:
Type aliases, through "using" statement.
Chapter 3.2:
Inline keyword, prevent some frequently call tiny functions occupy to much stack.
Chapter 3.3:
As in chapter3.1, we make a aliases with vec3, color and points. In this part, it shows real usage of the aliases by using vec3 to provide color ppm.

Chapter 4.1:
Learn how to use a simple function to interprete rays.
Chapter 4.2:
Learn how to define a camera(or our eyes, or the player's angle of view in games) in ray tracing systems. Rays go out from the camera as an origin, project to the 2D panel.

Chapter 5.1:
Using a very direct way to connect geometry and linear algebra. Understanding how rays connects with spheres.
Chapter 5.2:
Placing a sphere which center at somewhere through Z-axis, the rays from our eye at origin will shadow a circle with an (0,0,z) origin and the same radius as the sphere.

Chapter 6.1：
Based on the discriminant of quadratic equation of unknown, we can determine whether the ray hit the sphere or not. Than we use (-b +- discriminant) / 2a to find out the hit point, than determine the color of the hit point pixel.
Chapter 6.2：
Replacing b with 2h in the roots calculation function.
Chapter 6.3:
If we need to consider a series of spheres, we need to abstract a class for all the spheres and also the hitted point.
Chapter 6.4:
If the ray and the normal face in the same direction, the ray is inside the object, if the ray and the normal face in the opposite direction, then the ray is outside the object. Use this way to determine which side the rays come from.
Chapter 6.5:
Realizing the hitted point class.
Chapter 6.6:
Share_ptr method, many pointers can use this to point at the same object.
Chapter 6.7:
For a relatively huge project, we can place all the constant value in the same header file.

Chapter 7.1:
Two ways of c and c++ to get real random number.
Chapter 7.2:
Antialiasing to make the edge smoother.

Chapter 8.1:
Rejection methods to get the point inside sphere. Randomly direction of rays from diffuse surface.
Chapter 8.2:
Limiting the max depth of recursion of the ray.
Chapter 8.3:
Use "Gamma-corrected" method to make the dark shadow under the sphere more easy to see.
Chapter 8.4:
Make t which is very closed to zero to excactly zero, in order to avoid shadow acne.
Chapter 8.5:
A slightly different method to deal with rays toward diffuse surface.
Chapter 8.6:
A more frequently used method to deal with rays toward diffuse surface in academic area.

Chapter 9.1:
New abstract class for different kinds of material which reflect rays.
Chapter 9.2:
Interacting with hittable.h and sphere.h to utilize methods.
Chapter 9.3:
Avoid wrong cases by define near-zero function for vec3 class. Use heritage to build lambertian class.
Chapter 9.4:
Metal material totally different from diffuse material, the incident light and emergent light are symmetric with the center of normal.
Chapter 9.5:
Show realization of metal meterial, which mirror the background world.
Chapter 9.6:
Use a sphere method just like we have done with diffuse material. In this case, the incident light and emergent light are not absolutely symmetric.

Chapter 10.1:
If we get all the rays refracted, it will produce a picture that different from reality.
Chapter 10.2:
Combining Snell's law and vector decomposition, we can get the method to calculate the refraction direction.
Chapter 10.3:
We need to consider whether a ray can be refracted or not, if it can not be refracted, it can only be reflected.
Chapter 10.4:
Schlick Approximation can be used solve the refraction of dielectric material in a simpler way.
Chapter 10.5:
If we make the radius parameter negative, it will produce a bubble to make a hollow glass sphere.

Chapter 11.1:
Without using default camera, we now move on to determine the camera by the position and angel with z-axis.
Chapter 11.2:
With the arbitrary camera, we can look at our object at different distances and angles.

Chapter 12.1:
Learning how a real camera works with lens, sensor and aperture. In our virtual camera, we don't need to care about light intensity so we ignore aperture.
Chapter 12.2:
Making me understand well what is so called "depth of field" and how we utilize this through defocus blur in ray tracing.







